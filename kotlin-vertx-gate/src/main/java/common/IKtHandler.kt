package common

/**
 * kt的业务处理接口
 * Created by JinMiao
 * 2017/11/7.
 */
interface IKtHandler {

    suspend fun handler(msg: Message)

    /**
     * 初始化消息体对象
     * @return
     */
    fun initBodyClass(): Any?

    /**TODO 获得线程名字  */
    fun getExecuteName(): String?
}