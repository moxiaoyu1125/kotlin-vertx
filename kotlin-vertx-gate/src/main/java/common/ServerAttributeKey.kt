package common

import io.netty.util.AttributeKey

object ServerAttributeKey {

    val channel_Id = AttributeKey.valueOf<Long>("channelId")

    val netChannel = AttributeKey.valueOf<NetChannel>("netChannel")


}
