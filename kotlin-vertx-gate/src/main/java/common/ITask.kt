package common

/**
 * Created by JinMiao
 * 2017/10/20.
 */
interface ITask {

    suspend fun run()

}