package common

import com.google.protobuf.GeneratedMessage

/**
 * 消息对象
 * @author pc
 */
class Message
/**
 * 在写业务的时候不要使用该方法方便扩展
 * 替代方案[Message.newMessage]
 */
private constructor() {
    /**模块id */
    private var mId: Int = 0
    /**业务id */
    private var hId: Int = 0
    /**回调id */
    var responseId: Int = 0
    /**消息体 */
    private var obj: Any? = null

    var channel: NetChannel? = null

    fun <T> getBody(): T {
        return obj as T
    }

    fun setBody(obj: Any) {
        this.obj = obj
    }


    fun setBody(builder: GeneratedMessage.Builder<*>) {
        this.obj = builder.build()
    }


    fun getmId(): Int {
        return mId
    }

    fun setmId(mId: Int) {
        this.mId = mId
    }

    fun gethId(): Int {
        return hId
    }

    fun sethId(hId: Int) {
        this.hId = hId
    }


    suspend fun call(){



    }
    companion object {
        /**模块 */
        const val M_ID = -1
        /**通知业务层断开连接了   在业务层注册该消息 */
        const val H_DISCONNET = 1
        /**发送心跳 */
        const val H_SEND_HEART_BEAT = 2
        /**接收心跳返回 */
        const val H_RECIEVE_HEART_BEAT = 2000


        /**消息最大 [Integer.MAX_VALUE]    消息头长度 4字节 */
        val MAX_MESSAGE_SIZE = Integer.MAX_VALUE
        val HEAD_SIZE = 4

        /**获取一个新message */
        @JvmStatic
        fun newMessage(): Message {
            //		MyMessage msg = threadLocal.get();
            //		if(msg==null)
            //		{
            //			msg = new MyMessage();
            //			threadLocal.set(msg);
            //		}else
            //			msg.clearDate();
            return Message()
            //		return new MyMessage();
            //		if(debug)
            //		{
            //			return new MyMessage();
            //		}
            //		message.clearData();
            //		return message;
        }
    }
}
