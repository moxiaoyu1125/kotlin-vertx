package common

import org.slf4j.LoggerFactory

/**
 * Created by JinMiao
 * 2017/11/13.
 */
abstract class ExceptionHandler :IKtHandler {




    suspend override fun handler(msg: Message) {
        try {
            execute(msg)
        }catch (e:Throwable){
            logger.error("handlerException", e)
        }
    }

    @Throws(Throwable::class)
    abstract suspend fun execute(msg:Message)

    override fun initBodyClass(): Any?=null

    override fun getExecuteName(): String?=null
    companion object {
        protected val logger = LoggerFactory.getLogger(ExceptionHandler::class.java!!)!!
    }
}