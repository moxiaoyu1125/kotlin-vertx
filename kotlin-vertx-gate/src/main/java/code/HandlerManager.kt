package code

import common.IKtHandler
import java.util.*


/**
 * 业务处理管理类
 * @author jinmiao
 */
class HandlerManager : IHandlerManager {

    private val handlerMap = HashMap<Int, MutableMap<Int, IKtHandler>>()


    /**
     * 增加一个业务处理类
     * @param mid
     * @param hid
     * @param handler
     */
    override fun addHandler(mid: Int, hid: Int, handler: IKtHandler) {
        var handlers = handlerMap[mid]
        if (handlers == null)
            handlers = HashMap()
        handlers.put(hid, handler)
        handlerMap.put(mid, handlers)
    }

    /**
     * 获取一个业务处理类
     * @param mid
     * @param hid
     * @return
     */
    override fun getHandler(mid: Int, hid: Int): IKtHandler? {
        val handlers = handlerMap[mid] ?: //			logger.error("modelId:"+modelId+"actionId:"+actionId+" not found");
                return null
        return handlers[hid]
    }
}
