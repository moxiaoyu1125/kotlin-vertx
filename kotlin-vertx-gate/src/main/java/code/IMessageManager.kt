package code

import com.google.protobuf.MessageLite

/**
 * 消息体管理器接口
 * @author King
 */
interface IMessageManager<M, out N> {
    /**
     * 获取消息体
     * @param msgCla
     * @param body
     * @return
     */
    fun getBody(
            msgCla: Class<M>, body: ByteArray): MessageLite?

    /**
     * 增加一个消息体对象
     * @param modelId
     * @param actionId
     * @param msgCla
     */
    fun addMessageCla(modelId: Int, actionId: Int,
                      msgCla: Class<M>)

    /**
     * 二进制转对象
     * @param modelId
     * @param actionId
     * @param body
     * @return
     */
    fun getMessage(modelId: Int, actionId: Int,
                   body: ByteArray): N?

    /**
     * 根据 moduleid和actionid获得消息对象
     * @param modelId
     * @param actionId
     * @return
     */
    fun getMessage(modelId: Int, actionId: Int): N?
}