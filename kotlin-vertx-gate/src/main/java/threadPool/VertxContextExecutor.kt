package threadPool

import common.IKtHandler
import common.Message
import io.vertx.core.Context
import io.vertx.kotlin.coroutines.dispatcher
import kotlinx.coroutines.experimental.CoroutineDispatcher
import kotlinx.coroutines.experimental.launch

/**
 * Created by JinMiao
 * 2017/11/10.
 */
class VertxContextExecutor
{
    private var coroutineDispatcher: CoroutineDispatcher

    constructor(context: Context) {
        coroutineDispatcher = context.dispatcher()
    }


    fun put(handler: IKtHandler,msg:Message){
        launch(coroutineDispatcher) {
            handler.handler(msg)
        }
    }





}