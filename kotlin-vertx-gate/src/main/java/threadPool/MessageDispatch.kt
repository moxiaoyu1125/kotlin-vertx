package threadPool

import common.BaseTask
import common.IKtHandler
import common.Message
import org.slf4j.LoggerFactory
import utils.sendTask

/**
 * Created by JinMiao
 * 2017/11/13.
 */
abstract class MessageDispatch {

    private lateinit var executorMap:MutableMap<String,VertxContextExecutor>

    abstract fun put(handler: IKtHandler?,message: Message)

    protected fun dispatchMessage(handler: IKtHandler?,message: Message){
        if(handler==null){
            log.error("no exist handler，mid:" + message.getmId() + "  " + message.gethId())
            return
        }
        //如果handler指定了线程 则在指定的线程创建一个协程去处理
        handler.getExecuteName()?.let {
            var executor = executorMap[it]
            executor!!.put(handler,message)
            return
        }
        //如果没有指定的话  看是不是actor模型
        message.channel?.actorJob?.let {
            it.sendTask(BaseTask(message,handler))
            return
        }
    }
    companion object {
        private val log = LoggerFactory.getLogger(MessageDispatch::class.java)
    }


}