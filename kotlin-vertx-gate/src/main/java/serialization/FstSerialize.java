package serialization;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.nustaq.serialization.FSTObjectInput;
import org.nustaq.serialization.FSTObjectOutput;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FstSerialize implements ISerialize {
	private static Logger log = LoggerFactory.getLogger(FstSerialize.class);

	@Override
	public byte[] serialize(Object obj) {
		FSTObjectOutput fstOut = null;
		try {
			ByteArrayOutputStream bytesOut = new ByteArrayOutputStream();
			fstOut = new FSTObjectOutput(bytesOut);
			fstOut.writeObject(obj);
			fstOut.flush();
			return bytesOut.toByteArray();
		} catch (Exception e) {
			throw new RuntimeException(e);
		} finally {
			if (fstOut != null)
				try {
					fstOut.close();
				} catch (IOException e) {
					log.error(e.getMessage(), e);
				}
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> T unSerialize(byte[] bytes, Class<T> t) {
		if (bytes == null || bytes.length == 0)
			return null;

		FSTObjectInput fstInput = null;
		try {
			fstInput = new FSTObjectInput(new ByteArrayInputStream(bytes));
			return (T) fstInput.readObject();
		} catch (Exception e) {
			throw new RuntimeException(e);
		} finally {
			if (fstInput != null)
				try {
					fstInput.close();
				} catch (IOException e) {
					log.error(e.getMessage(), e);
				}
		}
	}

}
